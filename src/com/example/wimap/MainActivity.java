package com.example.wimap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

/**
 * 	what and how:
 * 
 * 	!	scan for wifi
 * 	!	check if wifi's open
 * 	if open get user's location
 * 	send location to server
 * 	make map page
 * 	draw small translucent circles around areas with wifi.. radius depends on strength
 * 	check if sign in is required -> send HTTP request and check if it's redirected.
 * 
 * */


/**
 * TODO
 * 
 * 1: Send new wifi on different threads.
 * 2: send HTTP request and see if it's redirected to check whether of not the wifi requires sign in.  
 * 3: implement a sqlite db on the phone to store wifi points incase we dont have wifi..
 * 4: also figure out way to store the map somehow..
 * 
 * */
public class MainActivity extends Activity implements
				GooglePlayServicesClient.ConnectionCallbacks,
				GooglePlayServicesClient.OnConnectionFailedListener,
				LocationListener{
	
	EditText 			edit,
						locationDisplay;
	Button	 			b,
						sendButton;
	WifiManager 		mainWifi;
    WifiReceiver 		receiverWifi;
    List<ScanResult> 	wifiList, 
    					openAccessPointsList;
    StringBuilder 		sb = new StringBuilder();
    LocationClient		locClient;
    Location			myLoc;
    LocationRequest 	locationRequest;
    
    /**
     * Activity Methods
     * 
     * */

	@Override
	protected void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Log.d("JOSH", "MainActivit started" );
		
		edit 			= (EditText)findViewById(R.id.results);
		b 				= (Button)findViewById(R.id.button);
		mainWifi 		= (WifiManager) getSystemService(Context.WIFI_SERVICE);		
	    receiverWifi 	= new WifiReceiver();
	    locationDisplay = (EditText)findViewById(R.id.locationDisplay);
	    sendButton		= (Button)findViewById(R.id.sendButton);
	    
	    sendButton.setClickable(false);
	    sendButton.setTextColor(Color.parseColor("#ff0000"));
	    
	    edit.setTextSize(7);
	    edit.setText("scanning...");
	    
	    registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
	    mainWifi.startScan();
	    
	    
	    locClient 		= new LocationClient(this, this, this);
	    
	    startService(new Intent(this, ScanService.class));

	}
	
	@Override
	protected void onStart(){
		super.onStart();
		locClient.connect();
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		startService(new Intent(this, ScanService.class));
		
	}
	
	@Override
	protected void onStop(){
		
		super.onStop();
		locClient.disconnect();
		
	}
	
	/**
	 * Called When we "backspace" out of the app.
	 * */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		unregisterReceiver(receiverWifi);
		Intent i = new Intent(this, ScanService.class );
		this.startService(i);
	}

	/**
	 * My Methods
	 * 
	 * */

	public void startScan(View v){
		
		sendButton.setClickable(false);
		sendButton.setTextColor(Color.parseColor("#ff0000"));
		registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		mainWifi.startScan();
		edit.setText("Scanning...");
		myLoc = locClient.getLastLocation();
		locationDisplay.setText(myLoc.getLatitude() + "  " + myLoc.getLongitude());
		
	}
	
	@SuppressLint("NewApi")
	public void sendAccessPointDetails(ScanResult result){
		
		String security;
		
		if(result.capabilities.contains("WPA")) security = "WPA";
		else 									security = "OPEN";
		
		String url = "http://109.255.215.99/wimap/addWifi.php" ;
		
		/**
		 * Try to send the open access point location to the main server...
		 * if we cant save them for later and send them when we do have wifi.
		 * */
		
		
		/**
		 *  scan again and make sure it's still there DO THIS LATER GET THE FUND.S WORKING FIRST..
		 *  if it is send the POST request FOR NOW SEND A GET..
		 *  do this on another thread after a bit..
		 **/
		
		StrictMode.enableDefaults();
		try{
			
			HttpClient 	httpclient	= new DefaultHttpClient();
			HttpPost 	httppost 	= new HttpPost(url);
			
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
			nameValuePairs.add(new BasicNameValuePair("name", result.SSID));
			nameValuePairs.add(new BasicNameValuePair("lat" , Double.toString(myLoc.getLatitude())));
			nameValuePairs.add(new BasicNameValuePair("lng",  Double.toString(myLoc.getLongitude())));
			nameValuePairs.add(new BasicNameValuePair("sec", security));
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpclient.execute(httppost);
			
		}catch(ClientProtocolException e){
			Toast.makeText(this, "Client protocol exception", Toast.LENGTH_SHORT).show();
		}catch(IOException e){
			Toast.makeText(this, "IO exception", Toast.LENGTH_SHORT).show();
		}
		
		
	}
	
	class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {

        	sendButton.setClickable(true);
        	sendButton.setTextColor(Color.parseColor("#00ff00"));
        	
        	sb 			= new StringBuilder();
            wifiList 	= mainWifi.getScanResults();

            /**
             * 	Display results for the user
             */
            
            for(int i = 0; i < wifiList.size(); i++){
            	
                sb.append(i + "  " +  wifiList.get(i).SSID + " " + wifiList.get(i).BSSID);
                
                if(wifiList.get(i).capabilities.contains("WPA")){
                	sb.append("-BAD-");
                }else{
                	sb.append("!!!!");
                }

               
                sb.append(wifiList.get(i).capabilities);
                sb.append(wifiList.get(i).level);
                sb.append("\n");
                
            }
            
            edit.setText(sb);
            
            
            /**
             * Send results to the server..
             * We'l do this on another thread cos it's a lot of requests to
             * be sending on the main UI thread 
             * CHANGE THIS SO NO STRICT MODE!
             * */
           
	          
        }
        
        
    }
	
	/**
	 * Implemented Methods
	 * */
	@Override
	public void onConnected(Bundle dataBundle){
		
		Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
		myLoc = locClient.getLastLocation();
		locationDisplay.setText(myLoc.getLatitude() + "  " + myLoc.getLongitude());
		
		
		locationRequest = LocationRequest.create();
	    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	    locationRequest.setInterval(10000); // 10 second intervals
	    locationRequest.setFastestInterval(10000);
	    locClient.requestLocationUpdates(locationRequest, this);
		
	}	
	
	
	@Override
    public void onDisconnected() {
        // Display the connection status
        Toast.makeText(this, "Disconnected. Please re-connect.",
                Toast.LENGTH_SHORT).show();
    }
	
	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
	 
		 Toast.makeText(this, "Can't connect to location service", Toast.LENGTH_SHORT).show();
		 
	 }
	 
	@Override
	public void onLocationChanged(Location location) {
		myLoc = location;
		locationDisplay.setText(myLoc.getLatitude() + " , " + myLoc.getLongitude());
		
	}
	

	/**
	 * OnClicks
	 * */
	
	public void sendRequest(View v){
		
		/*
		
		 new Thread(new Runnable(){
				@Override
				public void run() {
					
					
					for(int i = 0; i < wifiList.size(); i++){
						
						sendAccessPointDetails(wifiList.get(i));
						
					}
					
				}}).start();	*/
		
	}
	
	public void startMap(View v){
		 
		 Intent i = new Intent(this, Map.class);
		 startActivity(i);
		 
	 }

}
