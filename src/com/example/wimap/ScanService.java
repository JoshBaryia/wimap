package com.example.wimap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

/**
 * if no connection to internet then we cant do much except store
 * the details.
 * create settings?!
 * */

public class ScanService extends IntentService implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener,
		LocationListener{

	WifiManager			mainWifi;
	WifiReceiver		receiverWifi;
	List<ScanResult> 	wifiList;
	LocationClient		locClient;
	Location 			myLoc;
	LocationRequest 	locationRequest;
	int 				callCount;
	/**
	 * scan every 30 seconds
	 * get location every ten
	 * send to the server.
	 * */
	
	public ScanService() {
		super("com.example.wimap.ScanService");

	}

	@Override
	public void onCreate(){
		super.onCreate();
		
		mainWifi 		= (WifiManager) getSystemService(Context.WIFI_SERVICE);		
	    receiverWifi 	= new WifiReceiver();
	    registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
	    mainWifi.startScan();
	    
	    locClient 		= new LocationClient(this, this, this);
		locClient.connect();
		
		callCount = 0;
		Log.d("JOSH", "service oncreate called");
		
		Timer timer = new Timer();
		TimerTask service;
		
		
		
//		service = new TimerTask(){
//			@Override
//			public void run() {
				mainWifi.startScan();
				Log.d("JOSH", "Service scanning" + callCount);
				callCount++;
	//		}};
		
	//	timer.schedule(service, 30000);
		
	}
	
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		Log.d("JOSH", "service onDestroy called");
		unregisterReceiver(receiverWifi);
		
	}
	
	@SuppressLint("NewApi")
	@Override
	protected void onHandleIntent(Intent i){
		
		
	}
	
	@SuppressLint("NewApi")
	public void sendAccessPointDetails(ScanResult result){
			
	//	StrictMode.enableDefaults();

			String security;
			
			if(result.capabilities.contains("WPA")) security = "WPA";
			else 									security = "OPEN";
			
			String url = "http://109.255.215.99/wimap/addWifi.php" ;
			
			/**
			 * Try to send the open access point location to the main server...
			 * if we cant save them for later and send them when we do have wifi.
			 * */
			
			
			/**
			 *  scan again and make sure it's still there DO THIS LATER GET THE FUND.S WORKING FIRST..
			 *  if it is send the POST request FOR NOW SEND A GET..
			 *  do this on another thread after a bit..
			 **/
			
			String urll = "http://109.255.215.99/wimap/test.php?q=" + callCount;
			Log.d("JOSH", "sending accesspoints");
		//	StrictMode.enableDefaults();
			try{
				
				HttpClient 	httpclient	= new DefaultHttpClient();
				HttpPost 	httppost 	= new HttpPost(urll);
				
//				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(4);
//				nameValuePairs.add(new BasicNameValuePair("name", result.SSID));
//				nameValuePairs.add(new BasicNameValuePair("lat" , Double.toString(myLoc.getLatitude())));
//				nameValuePairs.add(new BasicNameValuePair("lng",  Double.toString(myLoc.getLongitude())));
//				nameValuePairs.add(new BasicNameValuePair("sec", security));
				
//				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpclient.execute(httppost);
				
			}catch(ClientProtocolException e){
				
			}catch(IOException e){
				
			}
			
			
		}
	
	
	
	class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {

           wifiList 	= mainWifi.getScanResults();

       
           for(int i = 0; i < wifiList.size(); i++){
        	   sendAccessPointDetails(wifiList.get(i));
        	   Log.d("JOSH", "sending details!");
           }
            
        }
        
        
        
        
    }

	/**
	 * Implemented Methods
	 * */
	
	
	@Override
	public void onLocationChanged(Location location) {
		myLoc = location;
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onConnected(Bundle connectionHint) {
		myLoc = locClient.getLastLocation();
		locationRequest = LocationRequest.create();
	    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
	    locationRequest.setInterval(5000); // 5 second intervals
	    locationRequest.setFastestInterval(5000);
	    locClient.requestLocationUpdates(locationRequest, this);
	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
		
		
}
