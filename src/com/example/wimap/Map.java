package com.example.wimap;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
 
/**
 * TODO
 * 1: get locations using AsyncTask to avoid too much stress on UI thread
 * !2: only receive wifi details near you
 * 3: somehow cache wifi details so they're available offline
 * 4: instead of downloading all the details each time another radio button is checked we should download them in the onCreate
 * 	  and just redraw it from the onchange of the radio button.
 * 5: change so we send POSTS not GETS
 * 6: make a class that handles wifi.. this will be handy for storing general wifi information and make erything less crash-.. ey?
 * 
 * 7! create a method to pull in wifis and another one to draw them and store them in a class var to make things smoother.. also make a refresh method for shits and gigs
 * 8 before we have downloaded the wifi dets the user can't press any radio button
 * 9 add a map listener to only draw the markers on the map shown on screen
 * 
 * */

	/**
	 * Activity Methods
	 * */

public class Map extends FragmentActivity implements
								GooglePlayServicesClient.ConnectionCallbacks,
								GooglePlayServicesClient.OnConnectionFailedListener
								{
	
	private GoogleMap 			map;
	private BufferedReader 		reader;
	private LocationClient		mClient;
	private Wifi[]				wifiList;
	
	private int toShow;
	

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		startService(new Intent(this, ScanService.class));

		this.toShow = Wifi.ALL;
		map 		= ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFrag)).getMap();
		map.setMyLocationEnabled(true);
		
		mClient = new LocationClient(this, this, this);
		mClient.connect();
		
		new AsyncTask(){
			@Override
			protected Object doInBackground(Object... params) {
				
				getWifiDetails();
				return null;
			}

			@Override
			protected void onPostExecute(Object result) {
				super.onPostExecute(result);
				drawMarkers();
			}
			
			
			
		}.execute();
		
	//	new DownloadDetails().execute();
//		try
//		new DownloadDetails().execute(new URL(""));

	}
	
	@Override
	protected void onStop(){
		super.onStop();
		mClient.disconnect();
		
		startService(new Intent(this, ScanService.class));
		
	}
	
	@Override
	protected void onStart(){
		super.onStart();
		//mClient.connect();
	}
	
	/**
	 *  contact the server and get the wifi names and locations then draw them on the map :):).. 
	 *  later we'l draw circles.. for now pins :)..
	 * */
	@SuppressLint("NewApi")
	public void getWifiDetails(){
		
//		String 			url 		= "http://109.255.215.99/wimap/getWifi.php?myLat="+ mClient.getLastLocation().getLatitude() +
//																				"&myLong=" + mClient.getLastLocation().getLongitude();
				
		String 			url 		= "http://109.255.215.99/wimap/getWifi.php?myLat=53&myLong=-9";
		
		
		String 			line 		= "";
		String 			details		= "";
		
		BufferedReader 	reader;
		
		StrictMode.enableDefaults();
		
		try{
			HttpClient 		httpclient	= new DefaultHttpClient();
			HttpPost	 	httppost 	= new HttpPost(url);

			HttpResponse 	response 	= httpclient.execute(httppost);
			HttpEntity 		entity		= response.getEntity();
			InputStream 	i 			= entity.getContent();
			reader 						= null;
			reader 						= new BufferedReader(new InputStreamReader(i));
		
			while((line = reader.readLine()) != null){
			
				details = details.concat(line);
				
			}
			
			String[] 	profiles 	= details.split("~");
			wifiList 				= new Wifi[details.split("~").length];
			String[] 	det 		= new String[4];
			
			float 		lat,
						lng;
			
			for(int n = 0; n < profiles.length; n++){
				
				det[0] 		= profiles[n].split(">")[0].trim();
				det[1] 		= profiles[n].split(">")[1].trim();
				det[2] 		= profiles[n].split(">")[2].trim();
				det[3] 		= profiles[n].split(">")[3].trim();
				
				wifiList[n] = new Wifi( det[0],
										new LatLng(
													Float.parseFloat(det[1]),
													Float.parseFloat(det[2])
												),
										det[3]);
				
				lat 		= Float.parseFloat(det[1]);
				lng 		= Float.parseFloat(det[2]);
				
				
			}
			
		}catch(Exception e){
			Toast.makeText(this, "Exception e on http request", Toast.LENGTH_SHORT).show();
		}
		
	
	}
	
	public void drawMarkers(){
	
		map.clear();
		
		
		switch(this.toShow){
		
			case Wifi.OPEN:{
				
				for(int i = 0; i < wifiList.length; i++){
					if(wifiList[i].getSecurity() == Wifi.OPEN){
						map.addCircle(new CircleOptions()
									.center(wifiList[i].getLatLng())
									.radius(50)
									.fillColor(0x1000ff00)
									.strokeColor(Color.TRANSPARENT)
								);
					}
				}
				
				break;
			}
		
			case Wifi.WPA:{
				
				for(int i = 0; i < wifiList.length; i++){
					if(wifiList[i].getSecurity() == Wifi.WPA){
						map.addCircle(new CircleOptions()
									.center(wifiList[i].getLatLng())
									.radius(50)
									.fillColor(0x10ff0000)
									.strokeColor(Color.TRANSPARENT)
								);
					}
				}
				
				break;
			}
			
			case Wifi.ALL:{
				
				for(int i = 0; i < wifiList.length; i++){
					if(wifiList[i].getSecurity() == Wifi.WPA){
						map.addCircle(new CircleOptions()
									.center(wifiList[i].getLatLng())
									.radius(50)
									.fillColor(0x10ff0000)
									.strokeColor(Color.TRANSPARENT)
								);
						
					}else if(wifiList[i].getSecurity() == Wifi.OPEN){
									map.addCircle(new CircleOptions()
									.center(wifiList[i].getLatLng())
									.radius(50)
									.fillColor(0x1000ff00)
									.strokeColor(Color.TRANSPARENT)
								);
					}
					
				}
				break;
			}
			
			case Wifi.NONE:{
				break;
			}
		
		
		
		}
		
		
		
	}
	
	/**
	 * Listeners
	 * */
	
	public void radioListener(View v){
		
		boolean checked = ((RadioButton) v ).isChecked();
		
		RadioButton r = (RadioButton)findViewById(v.getId());

		map.clear();
		// if that button was already picked to nothing!
		// Somehow
		
		switch(v.getId()){
		
				case R.id.map_radio_all:
					if(checked){
						
						this.toShow = Wifi.ALL;
						
//						Toast.makeText(this, "All picked", Toast.LENGTH_SHORT).show();
						
							
					}
					break;
					
				case R.id.map_radio_open:
					if(checked){
				
						this.toShow = Wifi.OPEN;
						
//						Toast.makeText(this, "open picked", Toast.LENGTH_SHORT).show();
						
					}
					break;
					
				case R.id.map_radio_closed:
					if(checked){
						
						this.toShow = Wifi.WPA;
						drawMarkers();
//						Toast.makeText(this, "closed picked", Toast.LENGTH_SHORT).show();

					}
					break;
					
				case R.id.map_radio_none:
					if(checked){
						this.toShow = Wifi.NONE;drawMarkers();
					//	Toast.makeText(this, "None picked", Toast.LENGTH_SHORT).show();
					}
					break;
					
				default:{
					Toast.makeText(this, "RadioButton problem", Toast.LENGTH_SHORT).show();
				}
		
					
		}
		
		drawMarkers();
		
		//Toast.makeText(this, "end was hit", Toast.LENGTH_SHORT).show();
		
		
	}
	
	/**
	 * Implemented Methods
	 * */

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		
		Toast.makeText(this, "Couldn't connect", Toast.LENGTH_SHORT).show();
	}

	
	@Override
	public void onConnected(Bundle connectionHint) {

		Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show();
		
	}
	

	@Override
	public void onDisconnected() {
		Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
		
	}
	
	
}




