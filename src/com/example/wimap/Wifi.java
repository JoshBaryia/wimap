package com.example.wimap;

import com.google.android.gms.maps.model.LatLng;

public class Wifi {
	
	/**
	 * 	This class is for wifi objects.  it can hold usefull info about them and have some useful wifi methods.
	 * 
	 * */
	
	public static final int OPEN = 0; 
	public static final int WPA  = 1; 
	public static final int ALL	 = 2;	
	public static final int NONE = -1;
	
	/**
	 * When we draw the wifi what size do we want the radius to be?
	 * */
	public static final int CIRCLE_SIZE = 20;
	
	private String name;
	private LatLng 	position;
	private int security;

	public Wifi(String wifiName, LatLng wifiPosition, int security){
		
		this.name 		= wifiName;
		this.position 	= wifiPosition;
		this.security	= security;
		
	}
	
	/**
	 * Overloaded constructor to allow for string security instead of having to convert it in other classes.
	 * 
	 * */
	public Wifi(String wifiName, LatLng wifiPosition, String sec){
		
		this.name 		= wifiName;
		this.position 	= wifiPosition;
		
		if(sec.equals("WPA"))		this.security = Wifi.WPA;
		else if(sec.equals("OPEN")) this.security = Wifi.OPEN;
		
	}
	
	/**
	 *  Getters.
	 * */
	
	public LatLng getLatLng(){
		
		return this.position;
		
	}
	
	public String getName(){
		
		return this.name;
		
	}
	
	/**
	 * Check if all the details are right such as non blank name and right lat lng etc..
	 * */
	public boolean isValid(){
		
		boolean valid = true;
		
		if(this.name.equals("")) 			valid = false;
		if(this.position.latitude == 0) 	valid = false;
		if(this.position.longitude == 0) 	valid = false;
		if((this.security != WPA)
		  ||this.security != OPEN)			valid = false;
		
		return valid;
		
	}
	
	public int getSecurity(){
		
		return this.security;
		
	}
	
}














